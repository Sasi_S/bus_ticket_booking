const express = require('express');
const bodyparser = require('body-parser');
const app = express();
app.use(bodyparser.json());

const dotenv = require('dotenv');
const connectDB = require('./config/db');

//load config
dotenv.config({path: './config/config.env'});

connectDB();

//Route
app.use('/', require('./routes/index'));

app.listen(3000);