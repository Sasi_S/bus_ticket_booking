const mongoose = require('mongoose');

const BookingDetailschema = mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    gender : {
        type : String,
        required : true
    },
    mobile : {
        type : String,
        required : true
    },
    source : {
        type : String,
        required : true
    },
    destination : {
        type : String,
        required : true
    }
});

module.exports = mongoose.model('bookingdetails', BookingDetailschema);