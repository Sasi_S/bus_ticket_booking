const express = require('express');
const router = express.Router();

const bookingdetails = require('../models/bookingDetails');

//Get all Booking Details
router.get('/api/BookingDetails', (req, res) => {
    bookingdetails.find({}, (err, data) =>{
        if(!err){
            res.json(data);
        }
        else{
            console.log(err);
        }
    });
});

//Insertion
router.post('/api/bookingdetails/add', (req,res) =>{
    const details = new bookingdetails({
        name:req.body.name,
        gender : req.body.gender,
        mobile : req.body.mobile,
        source : req.body.source,
        destination : req.body.destination
    });
    details.save((err,data) =>{
        res.status(200).json({code:200, message : 'Booking Details added Successfully',
        addbookingdetails : data})
    });
});

//Get user details by id
router.get('/api/bookingdetails/:id',(req,res) =>{
    bookingdetails.findById(req.params.id, (err,data) =>{
        if(!err){
            res.send(data);
        }
        else{
            console.log(err);
        }
    });
});

//update
router.put('/api/bookingdetails/update/:id', (req,res) => {
    const details = {
        name:req.body.name,
        gender : req.body.gender,
        mobile : req.body.mobile,
        source : req.body.source,
        destination : req.body.destination
    };
    bookingdetails.findByIdAndUpdate(req.params.id, {$set:details}, {new:true}, (err,data) =>{
        if(!err){
            res.status(200).json({code:200, message : 'Booking Details updated Successfully',
            updatebookingdetails : data})
        }
        else{
            console.log(err);
        }
    });
});

//delete
router.delete('/api/bookingdetails/delete/:id', (req,res) => {
    bookingdetails.findByIdAndRemove(req.params.id, (err, data) => {
        if(!err){
            res.status(200).json({code:200, message : 'Booking Details deleted Successfully',
            deletebookingdetails : data})
        }
        else{
            console.log(err);
        }
    })
});
module.exports = router;